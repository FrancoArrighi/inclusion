using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public List<Persona> personas = new List<Persona>();
    bool cambiandoNivel = false;
    public GameObject txt_Ganaste;
    public static GameManager instancia;
    private void Awake()
    {
        instancia = this;
    }

    private void Update()
    {
        ComprobarEstadoPersonas();
    }
    public void ComprobarEstadoPersonas()
    {
        foreach (Persona persona in personas)
        {
            if (persona.enEstadoOptimo == false)
            {
                return;
            }
        }
        if (cambiandoNivel)
        {
            return;
        }
        StartCoroutine(PasarSiguienteNivel());
    }
    IEnumerator PasarSiguienteNivel()
    {
        cambiandoNivel = true;
        txt_Ganaste.SetActive(true);
        Time.timeScale = 0;
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
