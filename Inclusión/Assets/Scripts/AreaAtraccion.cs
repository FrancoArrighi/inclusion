using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AreaAtraccion : MonoBehaviour
{
    public Action TerminarAtraccion;

    public int contadorPersonasAtraidas = 0;
    public int maxPersonasEnArea = 4;
    public bool atraccionActiva;
    public float rangoAtraccion;

    public SpriteRenderer spr;
    public TMP_Text txt_personasEnRango;
    public Color[] coloresArea;

    Jugador jugador;

    void Start()
    {
        jugador = FindObjectOfType<Jugador>();
        spr = GetComponent<SpriteRenderer>();
        jugador.MostrarRangoAtraccion += LogicaAtraccion;
        SetearRangoAtraccion();
    }

    public void LogicaAtraccion()
    {
        atraccionActiva = !atraccionActiva;
        if (!atraccionActiva)
        {
            spr.color = coloresArea[0];
            DispararEventoTerminarAtraccion();
        }
        else
        {
            spr.color = coloresArea[1];
        }
    }

    private void Update()
    {
        txt_personasEnRango.text = contadorPersonasAtraidas.ToString() + "/" + maxPersonasEnArea.ToString();
    }

    public void SetearRangoAtraccion()
    {
        transform.localScale = new Vector2(rangoAtraccion, rangoAtraccion);
    }
    public void DispararEventoTerminarAtraccion()
    {
        TerminarAtraccion();
        contadorPersonasAtraidas = 0;
        atraccionActiva = false;
    }
    public bool MaxPersonasAtraidas()
    {
        if (contadorPersonasAtraidas < maxPersonasEnArea)
        {
            return false;
        }
        return true;
    }
}
