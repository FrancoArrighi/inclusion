using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;

public class Persona : MonoBehaviour
{
    private Rigidbody2D rb;

    public bool atraida = false;

    public TMP_Text txt_personas_faltantes_cerca;
    public Sprite[] spr_estados;
    public SpriteRenderer spr;
    public GameObject img_candado;

    public bool enEstadoOptimo = false;
    public float estado_optimo;
    public float rangoAmistad = 0;

    public bool bloqueda = false;
    public GameObject rango;

    public List<LineRenderer> lineas = new List<LineRenderer>();
    public GameObject contenedorLineas;
    public GameObject prefabLinea;

    Jugador jugador;
    AreaAtraccion areaAtraccion;

    private void Start()
    {
        areaAtraccion = FindObjectOfType<AreaAtraccion>();
        jugador = FindObjectOfType<Jugador>();
        rb = GetComponent<Rigidbody2D>();
        spr = GetComponent<SpriteRenderer>();
        rango = transform.GetChild(1).gameObject;

        OcultarRango();

        if (!bloqueda)
        {
            img_candado.SetActive(false);
            rb.constraints = RigidbodyConstraints2D.None;
        }
        else
        {
            img_candado.SetActive(true);
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
        }

        CrearLineas();
        jugador.MostrarRangoPersonas += MostrarOcultarRango;
        areaAtraccion.TerminarAtraccion += DesactivarAtraccion;
    }

    private void Update()
    {
        ChequearCantidadPersonasCerca();
    }

    public void ActivarAtraccion(Transform areaAtraccion)
    {
        this.transform.SetParent(areaAtraccion.transform);
        atraida = true;
    }

    public void DesactivarAtraccion()
    {
        if (bloqueda) { return; }
        this.transform.parent = null;
        atraida = false;
    }

    public void AsignarSprite(int index)
    {
        spr.sprite = spr_estados[index];
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("AreaAtraccion") && areaAtraccion.atraccionActiva && !areaAtraccion.MaxPersonasAtraidas()
            && !atraida && !bloqueda)
        {
            ActivarAtraccion(areaAtraccion.transform);
            areaAtraccion.contadorPersonasAtraidas++;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("AreaAtraccion") && areaAtraccion.atraccionActiva && atraida && !bloqueda)
        {
            DesactivarAtraccion();
            areaAtraccion.contadorPersonasAtraidas--;
        }
    }


    public void ChequearCantidadPersonasCerca()
    {
        int cantidad_personas_cerca = 0;

        foreach (Persona persona in GameManager.instancia.personas)
        {
            if (persona != this)
            {
                float distancia = Vector2.Distance(transform.position, persona.transform.position);
                float radioPersona = persona.transform.localScale.x / 2;
                if (distancia <= radioPersona + rangoAmistad * 0.25f)
                {
                    PosicionarLineRenderer(cantidad_personas_cerca, persona.transform);
                    cantidad_personas_cerca++;
                }
                else
                {
                    OcultarLineRenderer(cantidad_personas_cerca);
                }
            }
        }
        txt_personas_faltantes_cerca.text = cantidad_personas_cerca.ToString() + "/" + estado_optimo.ToString();
        ComprobarEstado(cantidad_personas_cerca);
    }
    public void ComprobarEstado(int cantidadPersonasCerca)
    {
        switch (cantidadPersonasCerca)
        {
            case int c when c <= estado_optimo * 0.25f:
                AsignarSprite(0);
                enEstadoOptimo = false;
                spr.color = Color.blue;
                break;
            case int c when c <= estado_optimo * 0.50f:
                AsignarSprite(1);
                enEstadoOptimo = false;
                spr.color = Color.gray;
                break;
            case int c when c <= estado_optimo * 0.75:
                AsignarSprite(2);
                enEstadoOptimo = false;
                spr.color = Color.yellow;
                break;
            case int c when c == estado_optimo:
                AsignarSprite(3);
                enEstadoOptimo = true;
                spr.color = Color.green;
                break;
            case int c when c > estado_optimo:
                AsignarSprite(0);
                enEstadoOptimo = false;
                spr.color = Color.blue;
                break;
            default:
                break;
        }
    }
    public void MostrarOcultarRango()
    {
        rango.transform.localScale = new Vector2(rangoAmistad, rangoAmistad);
        rango.SetActive(!rango.activeSelf);
    }
    public void OcultarRango()
    {
        rango.SetActive(false);
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, rangoAmistad * 0.25f);
    }
    public void CrearLineas()
    {
        for (int i = 0; i < GameManager.instancia.personas.Count - 1; i++)
        {
            GameObject linea = Instantiate(prefabLinea, this.transform.position, Quaternion.identity, contenedorLineas.transform);
            lineas.Add(linea.GetComponent<LineRenderer>());
            OcultarLineRenderer(i);
        }
    }

    public void PosicionarLineRenderer(int pos, Transform posFinal)
    {
        lineas[pos].gameObject.SetActive(true);
        lineas[pos].SetPosition(0, transform.position);
        lineas[pos].SetPosition(1, posFinal.position);
    }
    public void OcultarLineRenderer(int index)
    {
        lineas[index].gameObject.SetActive(false);
    }
}

