using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    public float velocidad = 0;

    public Action MostrarRangoPersonas;
    public Action MostrarRangoAtraccion;

    AreaAtraccion areaAtraccion;

    private void Start()
    {
        areaAtraccion = FindObjectOfType<AreaAtraccion>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MostrarRangoAtraccion();
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            MostrarRangoPersonas();
        }
    }

    private void FixedUpdate()
    {
        Mover();
    }


    private void Mover()
    {
        float movHorizontal = Input.GetAxisRaw("Horizontal") * velocidad;
        float movVertical = Input.GetAxisRaw("Vertical") * velocidad;

        Vector3 movementDirection = new Vector3(movHorizontal, movVertical, 0);

        transform.Translate(movementDirection * velocidad * Time.deltaTime, Space.World);
        areaAtraccion.transform.localPosition = Vector2.zero;
    }
}
